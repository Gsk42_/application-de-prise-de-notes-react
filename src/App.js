import './App.css';
import {Route, Routes, BrowserRouter, Link} from "react-router-dom";
import {ThemeProvider, useTheme} from "./components/theme.jsx"
import ToggleTheme from "./components/toggleMode";
import {
    Avatar,
    AvatarFallback,
    AvatarImage,
} from "./components/ui/avatar"
import {useState, useEffect} from "react";

import Note from "./components/Note";

import {Menu, SquarePen, Search, PanelRightOpen, Eye, Info, ListChecks, CircleEllipsis, Moon, Sun} from "lucide-react";


const newNote = async (state, usestate) => {
    const response = await fetch('http://localhost:3001/posts', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            title: 'Nouvelle note',
            content: 'Contenu de la nouvelle note',
            CreatedAt: new Date(),
            updatedAt: new Date()
        }),
    });
    const data = await response.json();
    console.log(data);
    usestate([...state, data]);
}

function App() {
    const [notes, setNotes] = useState([]);
    const [sidebarOpen, setSidebarOpen] = useState(false);
    useEffect(() => {
        fetch('http://localhost:3001/posts')
            .then(response => response.json())
            .then(data => {
                setNotes(data);
                console.log(data);
            });
    }, []);
    const {setTheme} = useTheme()
    console.log(setTheme, useTheme())
    return (
        <ThemeProvider defaultTheme="light" storageKey="theme">
            <BrowserRouter>
                <div className="flex h-screen">
                    <div className="bg-background w-full md:w-1/4 h-full"
                         style={{display: sidebarOpen ? 'block' : 'none'}}>
                        <div className="h-[4%] p-2 border-b border-r flex items-center justify-between">
                            <Menu/>
                            <span>
                          Toutes les notes
                      </span>
                            <SquarePen className={"w-6 h-6"} onClick={() => newNote(notes, setNotes)}/>
                        </div>
                        <div className="h-[3%] bg-background p-2 border-r border-b flex items-center justify-between">
                            <Search className={"w-4 h-4"}/>
                            <input type="text" placeholder="Rechercher" className="w-full mx-2"/>
                        </div>
                        <nav className="overflow-y-scroll overflow-x-hidden h-[93%]">
                            <ul>
                                {notes.map((note) => {
                                    return <li key={note.id}>
                                        <Note note={note}/>
                                    </li>
                                })}
                            </ul>
                        </nav>
                    </div>
                    <div className="flex flex-col w-full md:w-3/4" style={{width: sidebarOpen ? '75%' : '100%'}}>
                        <div className="bg-background  h-[4%]   border-r border-b flex items-center justify-between">
                            <div className="flex items-center h-full">
                                {!sidebarOpen ?
                                    <div className="border h-full w-full flex items-center justify-center">
                                        <SquarePen/>
                                    </div> : null
                                }
                                <PanelRightOpen className={"w-8 h-8"} onClick={() => setSidebarOpen(!sidebarOpen)}/>
                            </div>
                            <div className={"flex items-center justify-between p-2 rounded-lg w-[15%]"}>
                                <Eye className={"w-6 h-6"}/>
                                <Info className={"w-6 h-6"}/>
                                <ListChecks className={"w-6 h-6"}/>
                                <CircleEllipsis className={"w-6 h-6"}/>
                                <ToggleTheme/>
                            </div>
                        </div>
                        <div className="overflow-auto p-4 flex-grow mx-auto  w-3/4">
                            <div>
                                ldmjklfhjekzgjdlbkmnleùadmeznlkjm
                            </div>
                        </div>
                        <div className="bg-background border-r border-t" onClick={() => setTheme('dark')}>
                            footer
                        </div>
                    </div>
                </div>
            </BrowserRouter>
        </ThemeProvider>
    );
}

const routes = [
    {
        path: '/',
        component: <h1>Home</h1>,
        name: 'Home',
    },
    {
        path: '/about',
        component: <h1>About</h1>,
        name: 'About',
    },
];


export default App;
