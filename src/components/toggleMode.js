import {Sun, Moon} from 'lucide-react';
import {useTheme} from './theme.jsx';
import {useState} from 'react';

export default function ToggleMode() {
    const {setTheme} = useTheme();
    const [theme, setThemee] = useState(localStorage.getItem('theme') || 'light');
    const changeTheme = () => {
        const t = localStorage.getItem('theme')
        console.log(t)
        if (t === 'light') {
            localStorage.setItem('theme', 'dark')
            setThemee('dark')
            setTheme('dark')
        } else {
            localStorage.setItem('theme', 'light')
            setThemee('light')
            setTheme('light')
        }
    }
    return (
        <div className="w-6 h-6">
            {theme === 'light' ?
                <Moon className={"w-6 h-6"} onClick={() => changeTheme()}/> :
                <Sun className={"w-6 h-6"} onClick={() => changeTheme()}/>
            }
        </div>
    )
}