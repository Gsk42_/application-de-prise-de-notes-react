import {useState} from 'react';
import {Pin} from 'lucide-react';

const defaultNote = {
    title: 'Note Initiale',
    content: 'lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
};

function Note({note = defaultNote, pinned = false}) {
    const [isPinned, setIsPinned] = useState(pinned);
    const [isHovered, setIsHovered] = useState(false);
    return (
        <div className="border p-2 hover:bg-primary-foreground flex justify-evenly ">
            <div className="w-[5%] flex justify-center items-center" onMouseEnter={() => setIsHovered(true)}
                 onMouseLeave={() => setIsHovered(false)}>
                {isPinned || isHovered ? (
                    <div className="w-full flex justify-center" onClick={() => setIsPinned(!isPinned)}>
                        <Pin className={isPinned ? 'w-4 h-4 text-green-400' : 'w-4 h-4 text-red-500'}/>
                    </div>) : null}
            </div>
            <div className="w-[95%]">
                <h3 className=" font-bold">{note.title ? note.title : 'Sans titre'}</h3>
                <p className="w-11/12 text-xs overflow-hidden overflow-ellipsis whitespace-nowrap"
                >
                    {note.content} {isPinned}
                </p>
            </div>
        </div>
    )
}

export default Note;